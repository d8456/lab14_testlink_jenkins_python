#!/usr/bin/env bash

python3 setup.py build
echo ""
echo 'BUILT .SO SHARED OBJECT TO ./build/lib.[...]'
echo""
FILE=`find ./build -name 'main.cpython*'`
pyinstaller -F --add-binary "$FILE:./main2.so" app.py
echo ""
echo 'BUILT APP TO ./DIST'