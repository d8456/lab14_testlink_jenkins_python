# TestLink + Jenkins + Python
Laboratoria polegały na konfiguracji współpracy Jenkins'a i TestLink'a, tak aby Jenkins raportował wyniki testów TestLink'owi.
## Źródła kodu
Jako źródło kodu skonfigurowano publiczne repozytorium na prywatnym GitLab'ie.
## TestLink
Jenkinsowy plugin TestLink skonfigurowano podając niezbędne dane, m. in. *Niestandardowe pola* zawiera zmienną *robot*. Nazwa to pozostałość po implementacji tutoriala zaproponowanego do zadania - testowane są testy jednostkowe aplikacji z lab13, nie wykorzystano *robot framework*. Zmienna *robot* zdefiniowana dla testcase'ów w TestLink'u przechowuje identyfikator testowanej metody, dla każdego testcase'a.

## Jenskins - wykres testów
![f](mdAssets/jenkins_graph.png "f")
## Jenkins - raport testów
![f](mdAssets/jenkins_tests.png "f")
## TestLink - raport testów
![f](mdAssets/testlink_testcases.png "f")