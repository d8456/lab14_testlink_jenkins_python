//#include "main.h"

float u_cal(float u, int n)
{
    float temp = u;
    for (int i = 1; i < n; i++)
        temp = temp * (u - i);
    return temp;
}

int fact(int n)
{
    int f = 1;
    for (int i = 2; i <= n; i++)
        f *= i;
    return f;
}

extern "C" float calc(const float *xTab, const float *yTab, const int length, const float interpVal)
{
    float **y = new float*[length];
    for(int node = 0; node < length; ++node) {
        y[node] = new float[length];
        y[node][0] = yTab[node];
    }

    for (int i = 1; i < length; i++) {
        for (int j = 0; j < length - i; j++)
            y[j][i] = y[j + 1][i - 1] - y[j][i - 1];
    }

    // initializing u and sum
    float sum = y[0][0];
    float u = (interpVal - xTab[0]) / (xTab[1] - xTab[0]);
    for (int i = 1; i < length; i++) {
        sum = sum + (u_cal(u, i) * y[0][i]) /
                                 fact(i);
    }

    for(int node = 0; node < length; ++node) {
        delete y[node];
    }

    return sum;
}
