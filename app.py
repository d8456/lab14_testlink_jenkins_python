import newton
import sys
import ctypes, ctypes.util

def readData(filepath: str):
    x = []
    y = []
    with open(filepath) as file:
        while (line := file.readline()) != "\n":
            line = line.split(" ")
            x.append(float(line[0]))
            y.append(float(line[1]))

        inter = [float(val) for val in file.readline().split(" ")]

        return x, y, inter

def main():
    x, y, inter = readData(sys.argv[1])

    x_array_type = ctypes.c_float*len(x)
    x_array = x_array_type(*x)

    y_array_type = ctypes.c_float*len(y)
    y_array = y_array_type(*y)
    
    for val in inter:
        print("f(" + str(val) + ") = " + str(newton.calc(x_array,
            y_array, len(x), val)))


if __name__ == "__main__":
    main()