""" Python wrapper for the C shared library mylib"""
from lib2to3.pytree import Base
import sys, platform
import ctypes, ctypes.util

# Find the library and load it
try:
    mylib = ctypes.CDLL("./main2.so")
except OSError:
    print("Unable to load the system C library")
    sys.exit()

# Make the function names visible at the module level and add types
calc = mylib.calc

c_float_p = ctypes.POINTER(ctypes.c_float)

calc.argtypes = [c_float_p, c_float_p, ctypes.c_int, ctypes.c_float]
calc.restype = ctypes.c_float