import testlink
import tests
import datetime
import time
import sys

def main(url, devkey):
    start = time.time()
    result = tests.RunTest()
    secs = time.time() - start

    TESTLINK_API_PYTHON_SERVER_URL=url
    TESTLINK_API_PYTHON_DEVKEY=devkey
    testcaseExternalId = 'P01-4'
    dateformat = "%Y-%m-%d %H:%M"
    
    tlh = testlink.TestLinkHelper(TESTLINK_API_PYTHON_SERVER_URL,
        TESTLINK_API_PYTHON_DEVKEY)
    tls = testlink.TestlinkAPIClient(tlh._server_url, tlh._devkey,
        verbose=True)
        
    tc_info = tls.getTestCase(None, testcaseexternalid=testcaseExternalId)
    tc_info = tls.getProjectTestPlans('1')
    
    #zbieranie step'ow
    testSteps = [case for case in dir(tests.TestCalcMethod) if case.startswith("test")]
    successes = [suc.id().split(".")[-1] for suc in result.successes]
    steps = []
    for n, testStep in enumerate(testSteps):
        returnStep = {'step_number' : n+1, 'result' : 'p' if testStep in successes else 'f',
            'notes' : 'sukces' if testStep in successes else 'blad' }
        steps.append(returnStep)
        
    success = result.wasSuccessful()
    testcaseNote = 'prawidlowe dzialanie' if success else 'blad w dzialaniu biblioteki'
    resultCode = 'p' if success else 'f'
    tls.reportTCResult(None, 2, None, resultCode, testcaseNote, guess=True,
            testcaseexternalid=testcaseExternalId,
            platformname='linux/amd64',
            execduration=str(secs), timestamp=datetime.datetime.now().strftime(dateformat),
            steps=steps)

if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])