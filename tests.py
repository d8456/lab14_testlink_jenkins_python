import ctypes
import unittest
import newton
import os
import pprint

def tstVals(x, y, val):
    x_array_type = ctypes.c_float*len(x)
    x_array = x_array_type(*x)

    y_array_type = ctypes.c_float*len(y)
    y_array = y_array_type(*y)
    
    return newton.calc(x_array, y_array, len(x), val)

OK = 'ok'
FAIL = 'fail'
ERROR = 'error'
SKIP = 'skip'

class JsonTestResult(unittest.TextTestResult):
    def __init__(self, stream, descriptions, verbosity):
        super_class = super(JsonTestResult, self)
        super_class.__init__(stream, descriptions, verbosity)
        # TextTestResult has no successes attr
        self.successes = []

    def addSuccess(self, test):
        # addSuccess do nothing, so we need to overwrite it.
        super(JsonTestResult, self).addSuccess(test)
        self.successes.append(test)

    def json_append(self, test, result, out):
        suite = test.__class__.__name__
        if suite not in out:
            out[suite] = {OK: [], FAIL: [], ERROR:[], SKIP: []}
        if result is OK:
            out[suite][OK].append(test._testMethodName)
        elif result is FAIL:
            out[suite][FAIL].append(test._testMethodName)
        elif result is ERROR:
            out[suite][ERROR].append(test._testMethodName)
        elif result is SKIP:
            out[suite][SKIP].append(test._testMethodName)
        else:
            raise KeyError("No such result: {}".format(result))
        return out
 
    def jsonify(self):
        json_out = dict()
        for t in self.successes:
            json_out = self.json_append(t, OK, json_out)
        for t, _ in self.failures:
            json_out = self.json_append(t, FAIL, json_out)
        for t, _ in self.errors:
            json_out = self.json_append(t, ERROR, json_out)
        for t, _ in self.skipped:
            json_out = self.json_append(t, SKIP, json_out)
        return json_out

class TestCalcMethod(unittest.TestCase):

    def test_1_1_middle(self):
        x = [10, 20]
        y = [100, 200]
        val = 15

        self.assertEqual(tstVals(x, y, val), 150)

    def test_2_2_middle(self):
        x = [10, 20]
        y = [100, 200]
        val = 12.5

        self.assertEqual(tstVals(x, y, val), 125)
        
        val = 17.5
        self.assertEqual(tstVals(x, y, val), 175)

    def test_3_1_out(self):
        x = [10, 20]
        y = [100, 200]
        val = 30

        self.assertEqual(tstVals(x, y, val), 300)

    def test_4_2_out(self):
        x = [10, 20]
        y = [100, 200]
        val = 25

        self.assertEqual(tstVals(x, y, val), 250)
        
        val = 35
        self.assertEqual(tstVals(x, y, val), 350)

def RunTest():
    with open(os.devnull, 'w') as null_stream:
        # new a runner and overwrite resultclass of runner
        runner = unittest.TextTestRunner(stream=null_stream)
        runner.resultclass = JsonTestResult
        # create a testsuite
        suite = unittest.TestLoader().loadTestsFromTestCase(TestCalcMethod)
        # run the testsuite
        result = runner.run(suite)

        return result

if __name__ == '__main__':
    #unittest.main()
    RunTest()